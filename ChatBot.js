/**
 * @class
 */
class ChatBot {

    /**
     * @constructor
     */
    constructor() {
        this._responseMethodsByQuery = {};
        this._iDontKnowResponse = (query) => {
            return 'Sorry, I don\'t understand.';
        };
        this._synonymMapping = {};
        this._synonymIndex = 0;
    }

    /**
     * @public
     * @param {Function} f
     */
    setIDontKnowResponse(f) {
        this._iDontKnowResponse = f;

        return this;
    }

    /**
     * Returns the edit distance between two strings using the Wagner-Fischer algorithm.
     * @param {String} s
     * @param {String} t
     */
    static _editDistance(s, t) {
        const m = s.length;
        const n = t.length;
        let d = [];

        for (let i = 0; i < m; i++) {
            let row = [];
            
            for (let j = 0; j < n; j++) {
                row.push(0);
            }

            d.push(row);
        }

        for (let i = 0; i < m; i++) {
            d[i][0] = i;
        }

        for (let j = 0; j < n; j++) {
            d[0][j] = j;
        }

        for (let j = 1; j < n; j++) {
            for (let i = 1; i < m; i++) {
                if (s[i] === t[j]) {
                    d[i][j] = d[i-1][j-1];
                }
                else {
                    d[i][j] = Math.min(d[i-1][j] + 1, d[i][j-1] + 1, d[i-1][j-1] + 1);
                }
            }
        }

        return d[m - 1][n - 1];
    }

    static _cleanQuery(query, synonyms) {
        // Remove non-letters, make all lowercase.
        let result = query.replace(/[^\w\s]/g, '').toLowerCase();

        // Collapse characters repeated > 2 times in a row.
        for (let i = 0; i < result.length - 3; i++) {
            while (result[i] === result[i + 1] && result[i + 1] == result[i + 2]) {
                result = result.slice(0, i) + result.slice(i + 1, result.length);
            }
        }

        if (synonyms)
            result = this._reduceSynonyms(result, synonyms);

        return result;
    }

    static _reduceSynonyms(query, synonymMapping) {
        const synonyms = Object.keys(synonymMapping).sort((a, b) => {
            return b.length-a.length;
        });
        const tokens = query.split(' ');

        // Perform sliding-window fuzzy matching for synonyms.
        // TODO: Match and replace all.
        synonyms.forEach(nym => {
            const nymTokens = nym.split(' ');
            const windowSize = nymTokens.length;
            const replacement = synonymMapping[nym];

            for (let i = 0; i + windowSize <= tokens.length; i++) {
                const phrase = tokens.slice(i, i + windowSize).join(' ');

                if (this._fuzzyMatch(nym, phrase)) {
                    tokens.splice(i, windowSize, replacement);
                    break;
                }
            }
        });
        
        return tokens.join(' ');
    }

    _getResponse(query) {
        let closestMatch = null;
        let minDistance = Infinity;
        const cleanQuery = this.constructor._cleanQuery(query, this._synonymMapping);

        if (this._responseMethodsByQuery[cleanQuery]) return this._responseMethodsByQuery[cleanQuery](query);

        Object.keys(this._responseMethodsByQuery).forEach(knownQuery => {
            const distance = this.constructor._editDistance(cleanQuery, knownQuery);

            if (distance < minDistance) {
                minDistance = distance;
                closestMatch = knownQuery;
            }
        });

        if (!this.constructor._fuzzyMatch(cleanQuery, closestMatch)) {
            return this._iDontKnowResponse(query);
        }

        return this._responseMethodsByQuery[closestMatch](query);
    }

    static _fuzzyMatch(a, b) {
        const dist = this._editDistance(a, b);
        const size = (a.length + b.length) / 2.0;

        return (dist <= size * 0.25); 
    }

    /**
     * @return {String|ChatBot|Promise<String|ChatBot>}
     */
    handleQuery(query) {
        const response = this._getResponse(query);

        if (Array.isArray(response)) {
            return response[Math.floor(Math.random() * response.length)];
        }

        return response;
    }

    addSynonyms(synonymList) {
        let set = new Set();
        let i = 0;

        synonymList.forEach(nym => {
            set.add(ChatBot._cleanQuery(nym));
            this._synonymMapping[nym] = `__${this._synonymIndex}__`;
        });

        this._synonymIndex++;

        return this;
    }

    addQueryResponse(queries, method) {
        let newQueries = [];

        while (queries.length > 0) {
            const query = queries.shift();
            const qIndex = query.indexOf('?');

            if (qIndex > -1) {
                let spaceIndex = query.lastIndexOf(' ', qIndex);
                if (spaceIndex < 0) spaceIndex = 0;

                // Add query with word.
                const withWord = query.replace('?', '', 1);
                queries.push(withWord);

                // Add query without word.
                let withoutWord = query.slice(0, spaceIndex) + query.slice(qIndex + 1, query.length);
                withoutWord = withoutWord.trim();
                queries.push(withoutWord);

            } else {
                newQueries.push(query);
            }
        }

        newQueries.forEach(query => {
            this._responseMethodsByQuery[this.constructor._cleanQuery(query, this._synonymMapping)] = method;
        });

        return this;
    }
}
